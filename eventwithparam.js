const EventEmitter = require('events');
const emitter = new EventEmitter();

emitter.on('parameters', (arg) => {
	console.log('Listener called with parameters passed to it which are :',arg);
});

emitter.emit('parameters' , {id:1,url : 'http:\\', name: 'Megha'});